import { Kekspress, KekspressPassegnger, Keksception } from './kekspress';

// function testIntValues(){
//   const intMin = Number.
// }

const intValues: number[] = [
  Number.NaN,
  Number.NEGATIVE_INFINITY,
  Number.MIN_VALUE,
  Number.EPSILON,
  Number.MIN_SAFE_INTEGER,
  -1,
  0,
  1,
  20,
  111,
  Number.MAX_SAFE_INTEGER,
  Number.MAX_VALUE,
  Number.POSITIVE_INFINITY,
];

const stringValue: String[] = ['asdasd', '%', '/', '˘', "''", ')(', '=', '34134134134131'];

const passengers: KekspressPassegnger[] = [
  {
    name: 'Passenger-2',
    getOffAt: 1,
  },
  {
    name: 'Passenger-1',
    getOffAt: 0,
  },
  {
    name: 'Passenger-3',
    getOffAt: -1,
  },
  {
    name: 'Passenger-4',
    getOffAt: Number.POSITIVE_INFINITY,
  },
  {
    name: 'Passenger-5',
    getOffAt: Number.EPSILON,
  },
  {
    name: 'Passenger-6',
    getOffAt: Number.MAX_SAFE_INTEGER,
  },
  {
    name: 'Passenger-7',
    getOffAt: Number.MAX_VALUE,
  },
  {
    name: 'Passenger-8',
    getOffAt: Number.MIN_SAFE_INTEGER,
  },
  {
    name: 'Passenger-9',
    getOffAt: Number.MIN_VALUE,
  },
  {
    name: 'Passenger-10',
    getOffAt: Number.NEGATIVE_INFINITY,
  },
  {
    name: 'Passenger-11',
    getOffAt: Number.NaN,
  },
];

const callTimes = 10000;

describe('Kekspress', () => {
  describe('Initialization with int values', () => {
    intValues.forEach(intElement => {
      const kekspress = new Kekspress(intElement);
      test(`Train size: ${intElement} `, () => {
        expect(kekspress.maxSeats).toBe(intElement);
      });
      describe('getPassengers function tests', () => {
        test(`empty train with size: ${intElement}`, () => {
          const emptyPessengerArr: String[] = [];
          expect(kekspress.getPassengers()).toEqual(emptyPessengerArr);
        });
      });
      describe('Board function tests', () => {
        if (kekspress.maxSeats >= passengers.length) {
          test(`First train load with ${intElement}`, () => {
            jest.spyOn(kekspress, 'board');
            passengers.forEach(pElement => {
              const error = () => {
                kekspress.board(pElement.name, pElement.getOffAt);
              };
            });
            //expect(kekspress.board).toHaveBeenCalledTimes(passengers.length);
          });
          passengers.forEach(pe => {
            test(`alreaty borded`, () => {
              const error = () => {
                kekspress.board(pe.name, pe.getOffAt);
              };
              expect(error).toThrowError(Keksception);
            });
          });
        }
        passengers.forEach(pe => {
          test(`train Full at ${pe.name} and ${pe.getOffAt} with ${intElement}`, () => {
            const error = () => {
              kekspress.board(pe.name, pe.getOffAt);
            };
            expect(error).toThrowError(Keksception);
          });
        });
      });

      describe('getPassengers function tests 2', () => {
        if (
          kekspress.maxSeats === Number.MAX_VALUE ||
          kekspress.maxSeats === Number.POSITIVE_INFINITY ||
          kekspress.maxSeats === Number.MAX_SAFE_INTEGER ||
          kekspress.maxSeats === 111 ||
          kekspress.maxSeats === 20
        ) {
          test(`Non empty train with full size: ${intElement}`, () => {
            const PessengerArr: String[] = passengers.map(x => x.name);
            expect(kekspress.getPassengers()).toStrictEqual(PessengerArr);
          });
        } else if (
          kekspress.maxSeats === 1 ||
          kekspress.maxSeats === Number.EPSILON ||
          kekspress.maxSeats === Number.MIN_VALUE
        ) {
          test(`Non empty train with just one: ${intElement}`, () => {
            const PessengerArr: String[] = passengers.map(x => x.name);
            expect(kekspress.getPassengers()).toStrictEqual([PessengerArr[0]]);
          });
        } else {
          test(`Empty train with size: ${intElement}`, () => {
            expect(kekspress.getPassengers()).toStrictEqual([]);
          });
        }
      });

      describe('nextStop function tests', () => {
        intValues.forEach(() => {
          test(`nextStop called with skiping with property`, () => {
            const stop = kekspress.nextStop();
            //expect(stop).toEqual(passengers.reduce(x => x));
          });
        });
        test(`nextStop called with skiping without property`, () => {
          intValues.forEach(intVals => {
            kekspress.nextStop(intVals - 1);
          });

          //expect(kekspress.nextStop).toEqual(intValues.length);
        });
      });
      describe('getOff function tests', () => {
        if (kekspress.getPassengers().length === 0) {
          test(`Undefined getOff called ${passengers.length} times`, () => {
            passengers.forEach(pas => {
              const getoff = kekspress.getOff(pas.name);
              const asdd: Object = { getOffAt: -1, name: 'Passenger-3' };
              const asd: Object = { getOffAt: 0, name: 'Passenger-1' };
              if (getoff === undefined) {
                expect(getoff).toBe(undefined);
              } else if (!undefined && getoff === asd) {
                expect(getoff).toStrictEqual(asd);
                // expect(getoff).toStrictEqual([
                //   { getOffAt: 0, name: 'Passenger-1' },
                //   { getOffAt: -1, name: 'Passenger-3' },
                // ]);
              }
              //console.log(getoff);
            });
          });
        } else {
          test(`KekspressePassegner getOff called ${passengers.length} times`, () => {
            passengers.forEach(pas => {
              const getoff = kekspress.getOff(pas.name);
              expect(getoff).toEqual(
                passengers.splice(passengers.findIndex(passenger => passenger.name === name), 1)[0],
              );
              //console.log(getoff);
            });
          });
        }
      });
    });
  });
});
